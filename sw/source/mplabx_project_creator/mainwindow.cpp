#include "mainwindow.h"
#include "ui_mainwindow.h"
#include <QFileDialog>


MainWindow::MainWindow(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::MainWindow)
{
    ui->setupUi(this);

    ui->selectedPath->setText(QCoreApplication::applicationDirPath());
}

MainWindow::~MainWindow()
{
    delete ui;
}

void MainWindow::on_browseButton_clicked()
{
    QString currentDir = ui->selectedPath->text();
    QString dir = QFileDialog::getExistingDirectory(this, tr("Open Directory"),
                                                    currentDir,
                                                    QFileDialog::ShowDirsOnly
                                                    | QFileDialog::DontResolveSymlinks);
    ui->selectedPath->setText(dir);
}

void MainWindow::on_createButton_clicked()
{
    //QDir * targetDir = new QDir(ui->selectedPath->text());
    QDir targetDir(ui->selectedPath->text());

    targetDir.cdUp();

    QString currentDir = QCoreApplication::applicationDirPath();
    QFile::copy("/path/file", "/path/copy-of-file");
}
