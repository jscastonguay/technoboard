/* The MIT License (MIT)

Copyright (c) 2015 Jean-Sebastien Castonguay

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE. */

#include "technoboard.h"
#include "system.h"

static const unsigned int timer2Prescaler[PWM_TIMER_NB_OF_PERIOD] =
{T2_PS_1_1, T2_PS_1_8, T2_PS_1_64, T2_PS_1_256};

static const unsigned int timer3Prescaler[PWM_TIMER_NB_OF_PERIOD] =
{T3_PS_1_1, T3_PS_1_8, T3_PS_1_64, T3_PS_1_256};


static bool ConfigTimerSource (
        PWM_Output output,
        PWM_TimerSourcePeriod timerSourcePeriod) {

    switch (output) {
    case PWM_OUTPUT_P4_2:
        OpenTimer2(
                T2_ON |
                T2_IDLE_CON |
                T2_GATE_OFF |
                timer2Prescaler[timerSourcePeriod] |
                T2_32BIT_MODE_OFF |
                T2_SOURCE_INT,
                0xffff);
        break;
    case PWM_OUTPUT_P4_8:
    default:
        OpenTimer3(
                T3_ON |
                T3_IDLE_CON |
                T3_GATE_OFF |
                timer3Prescaler[timerSourcePeriod] |
                T3_SOURCE_INT,
                0xffff);
        break;
    }

    return true;
}


static inline unsigned int ComputeOCRValue( unsigned int dutyCycle, unsigned int pwmPeriod) {

    dutyCycle = MIN( dutyCycle, 100);
    return ((uint32_t)dutyCycle * (uint32_t)pwmPeriod) / 100;
}


bool PWM_Start(
        PWM_Output output,
        PWM_TimerSourcePeriod timerScrPeriod,
        unsigned int pwmPeriod,
        unsigned int dutyCycle) {

    unsigned int OCR;

    if (output >= PWM_NB_OF_OUTPUT) {
        return false;
    }

    if (timerScrPeriod >= PWM_TIMER_NB_OF_PERIOD) {
        return false;
    }

    ConfigTimerSource( output, timerScrPeriod);
    OCR = ComputeOCRValue( dutyCycle, pwmPeriod);
    
    switch (output) {
    case PWM_OUTPUT_P4_2:
    default:
        PPSOutput( PPS_RP8, PPS_OC1);
        CloseOC1();
        ConfigIntOC1( OC_INT_OFF);
        OpenOC1(OC_IDLE_CON |
                OC_TIMER2_SRC |
                OC_PWM_EDGE_ALIGN,

                OC_SYNC_TRIG_IN_CURR_OC,

                pwmPeriod, OCR);
        break;
    case PWM_OUTPUT_P4_8:
        PPSOutput( PPS_RP4, PPS_OC2);
        CloseOC2();
        ConfigIntOC2( OC_INT_OFF);
        OpenOC2(OC_IDLE_CON |
                OC_TIMER3_SRC |
                OC_PWM_EDGE_ALIGN,

                OC_SYNC_TRIG_IN_CURR_OC,

                pwmPeriod, OCR);
        break;
    }

    return true;
}


bool PWM_SetDutyCycle( PWM_Output output, unsigned int dutyCycle) {
    
    unsigned int OCR;

    if (output >= PWM_NB_OF_OUTPUT) {
        return false;
    }

    dutyCycle = MIN( dutyCycle, 100);

    switch (output) {
    case PWM_OUTPUT_P4_2:
    default:
        OCR = ComputeOCRValue( dutyCycle, ReadPeriodOC1PWM());
        SetDCOC1PWM( OCR, ReadPeriodOC1PWM());
        break;
    case PWM_OUTPUT_P4_8:
        OCR = ComputeOCRValue( dutyCycle, ReadPeriodOC2PWM());
        SetDCOC2PWM( OCR, ReadPeriodOC2PWM());
        break;
    }

    return true;
}


bool PWM_Stop( PWM_Output output) {

    if (output >= PWM_NB_OF_OUTPUT) {
        return false;
    }

    switch (output) {
    case PWM_OUTPUT_P4_2:
    default:
        PPSOutput( PPS_RP8, PPS_NULL);
        CloseOC1();
        break;
    case PWM_OUTPUT_P4_8:
        PPSOutput( PPS_RP4, PPS_NULL);
        CloseOC2();
        break;
    }

    return true;
}
