/* The MIT License (MIT)

Copyright (c) 2015 Jean-Sebastien Castonguay

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE. */

#include "technoboard.h"
#include "system.h"
#include "static_fifo.h"
#include "usb/usb_device_cdc.h"

#include <string.h>
#include <stdio.h>

#define STRING_SIZE  16
#define FIFO_SIZE    256

STATIC_FIFO_DEFINE_AND_CREATE( RxFifo, uint8_t, FIFO_SIZE);
STATIC_FIFO_DEFINE_AND_CREATE( TxFifo, uint8_t, FIFO_SIZE);

static UART_Port currentPort;


bool UART_Start(  UART_Baudrate baudrate, UART_Port port) {

    bool result = true;

    currentPort = port;

    switch (currentPort) {
        case UART_TXP3_2_RXP3_3:
            iPPSInput(IN_FN_PPS_U1RX, IN_PIN_PPS_RP15);
            iPPSOutput(OUT_PIN_PPS_RP6, OUT_FN_PPS_U1TX);
            break;
        case UART_TXP4_2_RXP4_3:
            iPPSInput(IN_FN_PPS_U1RX, IN_PIN_PPS_RP9);
            iPPSOutput(OUT_PIN_PPS_RP8, OUT_FN_PPS_U1TX);
            break;
        case UART_TXP4_8_RXP4_9:
            iPPSInput(IN_FN_PPS_U1RX, IN_PIN_PPS_RP3);
            iPPSOutput(OUT_PIN_PPS_RP4, OUT_FN_PPS_U1TX);
            break;
        default:
            result = false;
            break;
    }

    if (result) {
        CloseUART1();
        ConfigIntUART1( UART_RX_INT_EN | UART_RX_INT_PR6 | UART_TX_INT_EN | UART_TX_INT_PR6);
        OpenUART1( UART_EN | UART_BRGH_FOUR, UART_INT_TX_BUF_EMPTY | UART_TX_ENABLE, baudrate);
    }

    return result;
}


void UART_Stop() {

    CloseUART1();
    
    switch (currentPort) {
        case UART_TXP3_2_RXP3_3:
            iPPSOutput(OUT_PIN_PPS_RP6, OUT_FN_PPS_NULL);
            break;
        case UART_TXP4_2_RXP4_3:
            iPPSOutput(OUT_PIN_PPS_RP8, OUT_FN_PPS_NULL);
            break;
        case UART_TXP4_8_RXP4_9:
            iPPSOutput(OUT_PIN_PPS_RP4, OUT_FN_PPS_NULL);
            break;
        default:
            break;
    }
}


unsigned int UART_WriteData( uint8_t * data, unsigned int sizeToWrite) {

    unsigned int sizeWritten = 0;
    bool result = true;

    if (data != NULL) {
        while ((sizeWritten < sizeToWrite) && result) {
            INTERRUPT_PROTECT( STATIC_FIFO_PUSH(TxFifo, data[sizeWritten], result));
            if (result) {
                sizeWritten++;
            }
        }
    }

    if (sizeWritten > 0) {
        IFS0bits.U1TXIF = 1;
    }

    return sizeWritten;
}


unsigned int UART_PrintString( char * string) {
    return UART_WriteData( (uint8_t *)string, strlen(string));
}


unsigned int UART_PrintInt( int value) {

    char string[STRING_SIZE];

    snprintf( string, STRING_SIZE, "%d", value);
    return UART_PrintString( string);
}


unsigned int UART_PrintUInt( unsigned int value) {

    char string[STRING_SIZE];

    snprintf( string, STRING_SIZE, "%u", value);
    return UART_PrintString( string);
}


unsigned int UART_PrintHex( unsigned int value) {

    char string[STRING_SIZE];

    snprintf( string, STRING_SIZE, "0x%04X", value);
    return UART_PrintString( string);
}


unsigned int UART_ReadData( uint8_t * data, unsigned int sizeAvailable) {

    unsigned int sizeRead = 0;
    bool result = true;

    if (data != NULL) {
        while ((sizeRead < sizeAvailable) && result) {
            INTERRUPT_PROTECT( STATIC_FIFO_POP( RxFifo, data[sizeRead], result));
            if (result) {
                sizeRead++;
            }
        }
    }

    return sizeRead;
}


bool UART_IsDataReceived() {
    return !STATIC_FIFO_IS_EMPTY(RxFifo);
}


char UART_GetChar() {
    
    uint8_t data;

    UART_ReadData( &data, 1);

    return (char)data;
}


void UART_PutChar( char character) {
    UART_WriteData( (uint8_t *)(&character), 1);
}


void __attribute__ ((interrupt,no_auto_psv)) _U1TXInterrupt(void) {

    unsigned int data;
    bool result = true;

    U1TX_Clear_Intr_Status_Bit;

    while (U1STAbits.UTXBF == 0 && !STATIC_FIFO_IS_EMPTY(TxFifo) && result) {
        STATIC_FIFO_POP( TxFifo, data, result);
        WriteUART1((unsigned int)data);
    }
}


void __attribute__ ((interrupt,no_auto_psv)) _U1RXInterrupt(void) {

    unsigned int data;
    bool result = true;

    U1RX_Clear_Intr_Status_Bit;

    while (DataRdyUART1() && !STATIC_FIFO_IS_FULL(RxFifo) &&  result) {
        data = ReadUART1();
        STATIC_FIFO_PUSH(RxFifo, data, result);
    }
}
