/* The MIT License (MIT)

Copyright (c) 2015 Jean-Sebastien Castonguay

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE. */

#include "technoboard.h"
#include "system.h"

#define CS_TRIS  TRISBbits.TRISB5
#define CS_PIN   PORTBbits.RB5

typedef union {
    struct {
        uint16_t data     : 12;
        uint16_t shutdown : 1;
        uint16_t gain     : 1;
        uint16_t dontCare : 1;
        uint16_t channel  : 1;
    };
    uint16_t rawData;
} MCP4822Command;

typedef enum {
    SHUTDOWN,
    ACTIVE
} ShutdownMode;

typedef enum {
    MAX_VOLTAGE_4096_MV, // 4096 mV
    MAX_VOLTAGE_2048_MV  // 2048 mV
} Gain;


static bool dacStarted = false;


static void StartFrame() {
    CS_PIN = GPIO_LOW;
}


static void StopFrame() {
    CS_PIN = GPIO_HIGH;
}


void DAC_Start() {
    
    unsigned int config1;
    unsigned int config2;
    unsigned int config3;

    CS_TRIS = GPIO_OUTPUT;
    StopFrame();
    PPSOutput( PPS_RP5, PPS_SDO1);
    PPSOutput( PPS_RP6, PPS_SCK1OUT);

    CloseSPI1();

    ConfigIntSPI1( SPI_INT_DIS | SPI_INT_PRI_4);

    config1 = ENABLE_SCK_PIN
            | ENABLE_SDO_PIN
            | SPI_MODE16_ON
            | SPI_SMP_OFF
            | SPI_CKE_OFF
            | CLK_POL_ACTIVE_LOW
            | MASTER_ENABLE_ON
            | SEC_PRESCAL_8_1
            | PRI_PRESCAL_64_1;
    config2 = FRAME_ENABLE_OFF;
    config3 = SPI_ENABLE
            | SPI_IDLE_CON
            | SPI_RX_OVFLOW_CLR;


    OpenSPI1( config1, config2, config3);

    dacStarted = true;
}


bool DAC_SetRawValue(DAC_Output output, unsigned int rawValue) {

    MCP4822Command command;
    bool result;
    unsigned int dummy;

    if (dacStarted == false) {
        DAC_Start();
    }
    
    if (output < DAC_NB_OF_OUTPUT) {
        
        rawValue = MIN( rawValue, DAC_MAX_RAW_VALUE);

        command.data = rawValue;
        command.shutdown = ACTIVE;
        command.gain = MAX_VOLTAGE_4096_MV;
        command.channel = output;

        StartFrame();
        WriteSPI1( command.rawData);
        while (SPI1_Tx_Buf_Full){;}
        while (!SPI1_Rx_Buf_Full){;}
        dummy = ReadSPI1();
        StopFrame();
        result = true;
    } else {
        result = false;
    }

    return result;
}


void DAC_Stop() {
    DAC_SetRawValue( DAC_P2_1, 0);
    DAC_SetRawValue( DAC_P2_2, 0);
    CloseSPI1();
    PPSOutput( PPS_RP5, PPS_NULL);
    PPSOutput( PPS_RP6, PPS_NULL);

    dacStarted = false;
}
