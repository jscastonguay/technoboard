/* The MIT License (MIT)

Copyright (c) 2015 Jean-Sebastien Castonguay

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE. */

#include "technoboard.h"
#include "driver_timer.h"
#include "system.h"
#include <GenericTypeDefs.h>

#define TIMER_MAX_VALUE          0xffffffffU
#define TIMER_TICK_PERIOD_IN_MS  10

/* Division by 8 since T1_PS_1_8 setting. Division by 2 since peripheral clock
 * (i.e. TCY = FOSC / 2). */
#define TIMER_CLK_PER_MS  (SYSCLK / (2 * 8 * MS_PAR_S))
#define TIMER_CLK_PER_TICK  ((TIMER_CLK_PER_MS * TIMER_TICK_PERIOD_IN_MS) - 1)

#if (TIMER_CLK_PER_TICK > 0xFFFF)
#error "__FILE__: TIMER_CLK_PER_TICK exceeds 16 bits. "
#endif

static volatile Timer_ms timerTick_ms = 0;


void DriverTimer_Init() {
    ConfigIntTimer1( T1_INT_PRIOR_4 | T1_INT_ON);
    OpenTimer1( T1_ON | T1_PS_1_8 | T1_SOURCE_INT, TIMER_CLK_PER_TICK);
}


Timer_ms Timer_GetTime() {

    Timer_ms TickCopy;

    INTERRUPT_PROTECT( TickCopy = timerTick_ms);

    return TickCopy;
}


Timer_ms Timer_GetElapseTime( Timer_ms referenceTime) {

    Timer_ms elapseTime;
    Timer_ms currentTime = Timer_GetTime();

    if (currentTime >= referenceTime) {
        elapseTime = currentTime - referenceTime;
    } else {
        elapseTime = (TIMER_MAX_VALUE - referenceTime) + currentTime + 1;
    }


    return elapseTime;
}


static inline void WaitOneMS() {

    Timer_ms startingTick = Timer_GetTime();
    
    uint32_t timerClkToWaitFor = (uint32_t)ReadTimer1() + TIMER_CLK_PER_MS;
    uint32_t WrapAroudOffset = 0;

    do {
        if (Timer_GetTime() != startingTick) {
            WrapAroudOffset = TIMER_CLK_PER_TICK;
        }
    } while ((ReadTimer1() + WrapAroudOffset) < timerClkToWaitFor);
}


void Timer_Wait( Timer_ms delay) {
    while (delay) {
        delay--;
        WaitOneMS();
    }
}


void __attribute__((__interrupt__, __shadow__, __auto_psv__)) _T1Interrupt(void) {
    T1_Clear_Intr_Status_Bit;
    timerTick_ms += TIMER_TICK_PERIOD_IN_MS;
}

