#include <stdio.h>
#include <stdlib.h>
#include "technoboard.h"

#define PMODACL_ADDR  0x1D
#define ACL_DATA_ADDR 0x32
#define ACL_DATA_SIZE 6

uint8_t raw[ACL_DATA_SIZE];

int main(int argc, char** argv) {

    Timer_ms lastOutputTime = 0;

    TechnoBoard_Initialise(TECHNOBOARD_ALL_FEATURES);
    I2C_Start(I2C_100KHZ);

    I2C_SendStart( PMODACL_ADDR, I2C_WRITE);
    I2C_Write(0x2D);
    I2C_Write(0x08);
    I2C_SendStop();
    
    while (true) {

        TechnoBoard_CyclicProcess();

        if (Timer_GetElapseTime(lastOutputTime) > 1000) {
            
            uint8_t id;
            int i;

            lastOutputTime = Timer_GetTime();
            
            I2C_SendStart( PMODACL_ADDR, I2C_WRITE);
            I2C_Write(0x00);
            I2C_SendRestart(I2C_READ);
            id = I2C_Read();
            I2C_SendStop();

            I2C_SendStart( PMODACL_ADDR, I2C_WRITE);
            I2C_Write(ACL_DATA_ADDR);
            I2C_SendRestart(I2C_READ);
            I2C_ReadData( raw, ACL_DATA_SIZE);
            I2C_SendStop();

            USB_PrintString("Device ID: ");
            USB_PrintInt( id);
            USB_PrintString(" status: ");
            USB_PrintInt( I2C_GetStatus());
            USB_PrintString("\n\r");
            USB_PrintString("ACL: ");
            for (i = 0; i < ACL_DATA_SIZE; i += 2) {
                int16_t acl;

                acl = raw[i] | (raw[i + 1] << 8);
                USB_PrintInt( acl);
                USB_PrintString(" ");
            }
            USB_PrintString("\n\r");
        }
        
    }

    return (EXIT_SUCCESS);
}

