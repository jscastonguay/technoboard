/* The MIT License (MIT)

Copyright (c) 2015 Jean-Sebastien Castonguay

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE. */

#include "technoboard.h"
#include "driver_pmodgyro.h"
#include "pid.h"

#define DISTANCE_BY_INTERRUPT  1.85 // unit: mm/interrupt

#define KP    128//512//4096
#define KI    10//40
#define KD    0//2048
#define IMAX  102400
#define IMIN  0

#define MOVING_COMMAND   700  // mm/s
#define TURNING_COMMAND  100  // mm/s
#define DEPLACEMENT      1000 // interrupts

#define PWM_LEFT    PWM_OUTPUT_P4_2
#define PWM_RIGHT   PWM_OUTPUT_P4_8
#define IC_LEFT     IC_MODULE_1
#define IC_RIGHT    IC_MODULE_2

typedef enum {
    IDLE,
    WAIT_BEFORE_MOVING_FORWARD,
    MOVING_FORWARD,
    WAIT_BEFORE_TURNING,
    TURNING,
    WAIT_BEFORE_MOVING_BACKWARD,
    MOVING_BACKWARD,
    END
} State;

static State  state = IDLE;
static Timer_ms time = 0;
static Timer_ms wait = 0;

static uint32_t displacementLeft;
static uint32_t displacementRight;
static int32_t angle = 0;

static Pid pidLeft;
static Pid pidRight;


static void DisplacementLeft() {
    displacementLeft++;
}


static void DisplacementRight() {
    displacementRight++;
}

static void StartMoving() {
    Pid_Init( &pidLeft, KP, KI, KD, IMAX, IMIN);
    Pid_Init( &pidRight, KP, KI, KD, IMAX, IMIN);
    PWM_Start( PWM_LEFT, PWM_TIMER_AT_4_us, 125, 0);
    PWM_Start( PWM_RIGHT, PWM_TIMER_AT_4_us, 125, 0);
    IC_Start( IC_LEFT, IC_INPUT_P4_3, IC_FALLING_AND_RISING_EDGES);
    IC_Start( IC_RIGHT, IC_INPUT_P4_9, IC_FALLING_AND_RISING_EDGES);
    displacementLeft = 0;
    displacementRight = 0;
    IC_InstallEventHandler(IC_LEFT, DisplacementLeft);
    IC_InstallEventHandler(IC_RIGHT, DisplacementRight);
}


static void StopMoving() {
    PWM_Stop( PWM_LEFT);
    PWM_Stop( PWM_RIGHT);
    IC_Stop( IC_LEFT);
    IC_Stop( IC_RIGHT);
}


static int ComputeSpeed( uint32_t delta) {
    
    float f = 1.0 / ((float)delta * IC_RESULUTION_IN_S);

    return (int)(DISTANCE_BY_INTERRUPT * f);
}


static void UpdateSpeed( Pid * pid, PWM_Output output, IC_Module input, int32_t command) {

    uint32_t delta = 0;
    int speed;
    int dc;


    if (IC_NewEventDeltaTimeAvailable(input)) {
        delta = IC_GetEventsDeltaTime(input);
        speed = ComputeSpeed( delta);
    } else {
        speed = 0;
    }
    
    dc = Pid_Update( pid, command, speed);

    dc = MAX( dc, 0);
    dc = MIN( dc, 100);

    PWM_SetDutyCycle( output, (unsigned int)dc);
}


int main(int argc, char** argv) {

    TechnoBoard_Initialise(TECHNOBOARD_ALL_FEATURES);
    Button_Init();
    DriverPmodgyro_Init();
    GPIO_P4_1_DIR = GPIO_OUTPUT;
    GPIO_P4_7_DIR = GPIO_OUTPUT;
    
    GPIO_P4_1_PIN = GPIO_LOW;
    GPIO_P4_7_PIN = GPIO_HIGH;

    while (true) {

        TechnoBoard_CyclicProcess();

        if (Timer_GetElapseTime(time) >= 10) {

            time = Timer_GetTime();

            switch (state) {

                case IDLE:
                    if (BUTTON_STATE == BUTTON_PRESSED) {
                        state++;
                        wait = Timer_GetTime();
                    }
                    break;

                case WAIT_BEFORE_MOVING_FORWARD:
                    if (Timer_GetElapseTime(wait) >= 2000) {
                        state++;
                        StartMoving();
                    }
                    break;

                case MOVING_FORWARD:
                    UpdateSpeed( &pidLeft, PWM_LEFT, IC_LEFT, MOVING_COMMAND);
                    UpdateSpeed( &pidRight, PWM_RIGHT, IC_RIGHT, MOVING_COMMAND);
                    if ((displacementRight > 4000) || (displacementLeft > DEPLACEMENT)) {
                        state++;
                        StopMoving();
                        wait = Timer_GetTime();
                    }
                    break;

                case WAIT_BEFORE_TURNING:
                    if (Timer_GetElapseTime(wait) >= 2000) {
                        state++;
                        GPIO_P4_1_PIN = GPIO_HIGH;
                        StartMoving();
                    }
                    break;

                case TURNING:
                    {
                    int32_t omega;

                    UpdateSpeed( &pidLeft, PWM_LEFT, IC_LEFT, TURNING_COMMAND);
                    UpdateSpeed( &pidRight, PWM_RIGHT, IC_RIGHT, TURNING_COMMAND);
                    omega = DriverPmodgyro_GetZ();
                    angle += (omega * DRIVER_PMODGYRO_CONVERSION) / 100;

                    if ((angle < -180000) || (angle > 180000)) {
                        state++;
                        StopMoving();
                        GPIO_P4_1_PIN = GPIO_LOW;
                        wait = Timer_GetTime();
                    }
                    }
                    break;

                case WAIT_BEFORE_MOVING_BACKWARD:
                    if (Timer_GetElapseTime(wait) >= 2000) {
                        state++;
                        StartMoving();
                    }
                    break;

                case MOVING_BACKWARD:
                    UpdateSpeed( &pidLeft, PWM_LEFT, IC_LEFT, MOVING_COMMAND);
                    UpdateSpeed( &pidRight, PWM_RIGHT, IC_RIGHT, MOVING_COMMAND);
                    if ((displacementRight > DEPLACEMENT) || (displacementLeft > DEPLACEMENT)) {
                        state++;
                        StopMoving();
                        wait = Timer_GetTime();
                    }
                    break;
                    
                case END:
                default:
                    break;
            }
        }

    }

    return (EXIT_SUCCESS);
}
