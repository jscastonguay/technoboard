/* The MIT License (MIT)

Copyright (c) 2015 Jean-Sebastien Castonguay

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE. */

#include <stdio.h>
#include <stdlib.h>
#include "technoboard.h"

#define PI 3.1416
#define RADIUS_STRING_LENGHT  16

char radiusString[RADIUS_STRING_LENGHT];
unsigned int numberOfCharReceived = 0;

int main(int argc, char** argv) {

    numberOfCharReceived = 0;

    TechnoBoard_Initialise(TECHNOBOARD_ALL_FEATURES);
    numberOfCharReceived = 0;

     UART_Start( UART_115200, UART_TXP4_2_RXP4_3);

    while (true) {

        TechnoBoard_CyclicProcess();

        if (USB_IsDataReceived()) {

            char charReceived = USB_GetChar();

            UART_PutChar(charReceived);

            USB_PrintString(" nb:");
            USB_PrintInt(numberOfCharReceived);
            USB_PrintString(" ");

            if ((charReceived == '\r') || (charReceived == '\n')) {

                if (numberOfCharReceived > 0) {
                    int radius;
                    float circumference;

                    USB_PrintString("\n\r");
                    radiusString[numberOfCharReceived] = '\0';
                    USB_PrintString(" ");
                    USB_PrintString(radiusString);
                    USB_PrintString(" ");
                    USB_PrintInt(numberOfCharReceived);
                    USB_PrintString(" ");
                    
                    radius = atoi(radiusString);
                    circumference = 2 * radius * PI;
                    USB_PrintString("The circumference of circle with a radius of ");
                    USB_PrintInt(radius);
                    USB_PrintString(" is ");
                    USB_PrintNumber(circumference, STR_FORMAT_FLOAT);
                    USB_PrintString("\n\r");
                }

                numberOfCharReceived = 0;

            } else {
                radiusString[numberOfCharReceived] = charReceived;
                numberOfCharReceived++;
            }
        }
    }

    return (EXIT_SUCCESS);
}

