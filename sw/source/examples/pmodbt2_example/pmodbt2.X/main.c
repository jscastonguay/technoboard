#include "technoboard.h"


int main(int argc, char** argv) {

    TechnoBoard_Initialise(TECHNOBOARD_ALL_FEATURES);
    UART_Start( UART_115200, UART_TXP4_2_RXP4_3);

    while (true) {
        
        TechnoBoard_CyclicProcess();

        if (UART_IsDataReceived()) {
            uint8_t data = (uint8_t)UART_GetChar();
            UART_PutChar( data + 1);
        }
    }

    return (EXIT_SUCCESS);
}

