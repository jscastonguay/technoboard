PCBNEW-LibModule-V1  jeu 19 jun 2014 14:12:10 EDT
# encoding utf-8
Units mm
$INDEX
ATS080B
$EndINDEX
$MODULE ATS080B
Po 0 0 0 15 53A327F4 00000000 ~~
Li ATS080B
Cd Condensateur = 2 pas
Kw C
Sc 0
AR 
Op 0 0 0
T0 0 0 1.016 1.016 0 0.2032 N V 21 N "ATS080B"
T1 0 0 1.016 1.016 0 0.2032 N I 21 N "X***"
DS -5.588 2.286 5.588 2.286 0.15 21
DS 5.588 2.286 5.588 -2.286 0.15 21
DS 5.588 -2.286 -5.588 -2.286 0.15 21
DS -5.588 -2.286 -5.588 2.286 0.15 21
$PAD
Sh "1" C 1.397 1.397 0 0 0
Dr 0.8128 0 0
At STD N 00E0FFFF
Ne 0 ""
Po -2.54 0
$EndPAD
$PAD
Sh "2" C 1.397 1.397 0 0 0
Dr 0.8128 0 0
At STD N 00E0FFFF
Ne 0 ""
Po 2.54 0
$EndPAD
$SHAPE3D
Na "discret/capa_2pas_5x5mm.wrl"
Sc 1 1 1
Of 0 0 0
Ro 0 0 0
$EndSHAPE3D
$EndMODULE ATS080B
$EndLIBRARY
